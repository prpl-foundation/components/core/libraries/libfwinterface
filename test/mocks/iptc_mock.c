/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <fcntl.h>
#include <linux/netfilter/xt_set.h>

#include <amxc/amxc_macros.h>

#include "iptc_mock.h"

#define IPSET_VERSION 7

struct xtc_handle {
    int dummy;
};

struct xtc_handle* __wrap_iptc_init(const char* table) {
    check_expected(table);
    return (struct xtc_handle*) calloc(1, sizeof(struct xtc_handle));
}

struct xtc_handle* __wrap_ip6tc_init(const char* table) {
    check_expected(table);
    return (struct xtc_handle*) calloc(1, sizeof(struct xtc_handle));
}

int __wrap_iptc_insert_entry(const xt_chainlabel chain, UNUSED const struct ipt_entry* e, UNUSED unsigned int rulenum, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_ip6tc_insert_entry(const xt_chainlabel chain, UNUSED const struct ipt_entry* e, UNUSED unsigned int rulenum, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_iptc_append_entry(const xt_chainlabel chain, UNUSED const struct ipt_entry* e, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_ip6tc_append_entry(const xt_chainlabel chain, UNUSED const struct ipt_entry* e, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_iptc_check_entry(const xt_chainlabel chain, UNUSED const struct ipt_entry* e, UNUSED unsigned char* matchmask, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_ip6tc_check_entry(const xt_chainlabel chain, UNUSED const struct ipt_entry* e, UNUSED unsigned char* matchmask, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_iptc_delete_num_entry(const xt_chainlabel chain, UNUSED unsigned int rulenum, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_ip6tc_delete_num_entry(const xt_chainlabel chain, UNUSED unsigned int rulenum, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

void __wrap_iptc_free(struct xtc_handle* h) {
    free(h);
}

void __wrap_ip6tc_free(struct xtc_handle* h) {
    free(h);
}

int __wrap_iptc_commit(UNUSED struct xtc_handle* handle) {
    return mock_type(int);
}

int __wrap_ip6tc_commit(UNUSED struct xtc_handle* handle) {
    return mock_type(int);
}

int __wrap_iptc_flush_entries(const xt_chainlabel chain, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_ip6tc_flush_entries(const xt_chainlabel chain, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_iptc_create_chain(const xt_chainlabel chain, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_ip6tc_create_chain(const xt_chainlabel chain, UNUSED struct xtc_handle* handle) {
    check_expected(chain);
    return mock_type(int);
}

int __wrap_socket(int domain, int type, int protocol) {
    assert_int_equal(domain, AF_INET);
    assert_int_equal(type, SOCK_RAW);
    assert_int_equal(protocol, IPPROTO_RAW);
    return 0;
}

void __wrap_close(UNUSED int fd) {
}

int __wrap_fcntl(UNUSED int fd, UNUSED int cmd, ...) {
    return 0;
}

int __wrap_getsockopt(UNUSED int fd, int level, int optname, void* optval, socklen_t* optlen) {
    struct ip_set_req_version* req_version = NULL;
    struct ip_set_req_get_set* req = NULL;
    int rv = -1;

    assert_int_equal(level, SOL_IP);
    assert_int_equal(optname, SO_IP_SET);

    if(*optlen == sizeof(struct ip_set_req_version)) {
        req_version = (struct ip_set_req_version*) optval;
        assert_int_equal(req_version->op, IP_SET_OP_VERSION);
        req_version->version = IPSET_VERSION;
        rv = 0;
    } else if(*optlen == sizeof(struct ip_set_req_get_set)) {
        req = (struct ip_set_req_get_set*) optval;
        assert_int_equal(req->op, IP_SET_OP_GET_BYNAME);
        assert_int_equal(req->version, IPSET_VERSION);
        check_expected(req->set.name);

        req->set.index = mock();
        rv = 0;
    }

    return rv;
}

int __wrap_file_open(UNUSED const char* filename) {
    return 100;
}

void __wrap_file_close(UNUSED int fd) {
}

int __wrap_file_access(UNUSED const char* filename) {
    return 0;
}

int __wrap_file_flock(UNUSED int fd) {
    return mock_type(int);
}