/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__INTERFACE_H__)
#define __INTERFACE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <stdbool.h>
#include <stdint.h>
#include <fwrules/fw_rule.h>

/**
   @defgroup interface
   @brief
   libfwinterface is a library for interfacing between libfwrules and different
   firewall systems in the Linux kernel, like netfilter or nftables.

   The interface functions are used to access the underlying firewall system.
   These functions must be implemented by each implementation.

   @attention
   For all API functions having a pointer to `fw_rule_t` as argument, a rule
   must have a table and chain set, otherwise an error (-1) is returned.

   @attention
   Depending on the implementation, all API functions that use a cache (to apply
   changes in a batch) require the process to sync via a lock file mechanism to
   avoid race conditions that would result in the kernel refusing to apply the
   outdated-cached-changes. The lock file is shared with iptables tools and, like
   in iptables, it can be configured with environment variable XTABLES_LOCKFILE.
   If it is not set, then the default lock file used will be "/run/xtables.lock".

   Header file to include:

   @verbatim
 #include <fwinterface/interface.h>
   @endverbatim
 */

/**
   @ingroup interface
   @brief
   Add a new chain to a firewall table.

   @note
   The chain is cached, to write to the firewall, invoke fw_apply().

   @param[in] chain The chain to add.
   @param[in] table The table where the chain is added to.
   @param[in] is_ipv6 @c true for an IPv6 chain, @c false for IPv4.

   @returns
   @c 0 on success, @c -1 on failure.
 */
int fw_add_chain(const char* chain, const char* table, bool is_ipv6);

/**
   @ingroup interface
   @brief
   Add a rule to the firewall, at at specific location in the rule's chain.

   @note
   The rule is cached, to write to the firewall, invoke fw_apply().

   @param[in] rule Pointer to a rule.
   @param[in] index The index in the chain where the rule is added to. Must be
   larger than 0.

   @returns
   @c 0 on success, @c -1 on failure.
 */

int fw_add_rule(const fw_rule_t* const rule, uint32_t index);

/**
   @ingroup interface
   @brief
   Replace a rule in the firewall, at a specific location in the rule's chain.

   @note
   The rule is cached, to write to the firewall, invoke fw_apply().

   @param[in] rule Pointer to a rule.
   @param[in] index The index in the chain where the rule is added to. Must be
   larger than 0.

   @returns
   @c 0 on success, @c -1 on failure.
 */
int fw_replace_rule(const fw_rule_t* const rule, uint32_t index);

/**
   @ingroup interface
   @brief
   Append a rule to the firewall at the end of the rule's chain.

   @note
   The rule is cached, to write to the firewall, invoke fw_apply().

   @param[in] rule Pointer to a rule.

   @returns
   @c 0 on success, @c -1 on failure.
 */
int fw_append_rule(const fw_rule_t* const rule);

/**
   @ingroup interface
   @brief
   Delete a rule from the firewall, at a specific location in the rule's chain.

   @note
   The rule is cached, to write to the firewall, invoke fw_apply().

   @param[in] rule Pointer to a rule.
   @param[in] index The index of the rule. Must be larger than 0.

   @returns
   @c 0 on success, @c -1 on failure.
 */
int fw_delete_rule(const fw_rule_t* const rule, uint32_t index);

/**
   @ingroup interface
   @brief
   The cached configuration is written to the firewall.

   This function must be invoked when:
   - One or multiple firewall rules are added, modified or deleted.
   - A firewall chain is added.

   When adding, modifying or deleting multiple firewall rules, it is sufficient
   to invoke this function one time at the end.

   @note
   Releases the lock file aqcuired by a previous call to:
   - \ref fw_add_chain
   - \ref fw_replace_rule
   - \ref fw_append_rule
   - \ref fw_delete_rule

   @returns
   @c 0 on success, @c -1 on failure.
 */
int fw_apply(void);

#ifdef __cplusplus
}
#endif

#endif // __INTERFACE_H__
