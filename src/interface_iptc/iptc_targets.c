/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <net/if.h>
#include <libiptc/libiptc.h>
#include <linux/netfilter/xt_NFQUEUE.h>
#include <linux/netfilter/xt_NFLOG.h>
#include <linux/netfilter/xt_LOG.h>

#include <fwrules/fw_rule.h>

#include "interface_iptc/iptc_includes.h"
#include "interface_iptc/iptc_targets.h"

#include <debug/sahtrace.h>
#include <amxc/amxc_macros.h>

#define typeof __typeof__

#define ME "firewall"

static const unsigned int target_size = XT_ALIGN(sizeof(struct xt_entry_target));

static MULTI_RANGE* multi_range_calloc(unsigned int* size) {
    MULTI_RANGE* mr = NULL;
    when_null(size, out);
    *size = XT_ALIGN(sizeof(MULTI_RANGE));
    mr = (MULTI_RANGE*) calloc(1, (size_t) *size);
out:
    return mr;
}

int iptc_rule_add_verdict_target(struct iptc_rule* r, int verdict) {
    struct xt_standard_target* target = NULL;
    unsigned int s = XT_ALIGN(sizeof(struct xt_standard_target));
    const char* label = "";

    when_null(r, out);

    target = (struct xt_standard_target*) calloc(1, s);
    when_null(target, out);

    switch(verdict) {
    case -NF_ACCEPT - 1:
        label = IPTC_LABEL_ACCEPT;
        break;
    case -NF_DROP - 1:
        label = IPTC_LABEL_DROP;
        break;
    case -NF_REPEAT - 1:
        label = IPTC_LABEL_RETURN;
        break;
    default:
        SAH_TRACEZ_WARNING(ME, "Lib_iptc uses the name to determine the type, when comparing this can be important");
        break;
    }

    target->target.u.target_size = s;
    strncpy(target->target.u.user.name, label, XT_EXTENSION_MAXNAMELEN);
    target->verdict = verdict;

    r->t.target = (struct ipt_entry_target*) target;
    r->target_size = s;
    r->target_list_size = 0;

out:
    return 1;

}

int iptc_rule_add_standard_target(struct iptc_rule* r, const char* t) {
    struct xt_standard_target* target = NULL;
    unsigned int s = XT_ALIGN(sizeof(struct xt_standard_target));

    when_null(r, out);

    target = (struct xt_standard_target*) calloc(1, s);
    when_null(target, out);

    target->target.u.target_size = s;
    snprintf(target->target.u.user.name, XT_EXTENSION_MAXNAMELEN, "%s", t);

    r->t.target = (struct ipt_entry_target*) target;
    r->target_size = s;
    r->target_list_size = 0;

out:
    return 1;
}

int iptc_rule_add_masquerade_target(struct iptc_rule* r, unsigned short minport, unsigned short maxport) {
    int retval = 0;
    MULTI_RANGE* mr = NULL;
    unsigned int mr_size = 0;

    when_null(r, out);

    mr = (struct nf_nat_ipv4_multi_range_compat*) multi_range_calloc(&mr_size);
    when_null(mr, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, error);

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "MASQUERADE", XT_EXTENSION_MAXNAMELEN);

    mr->rangesize = 1;

    /* choose port range */
    mr->range[0].flags |= IP_NAT_RANGE_PROTO_SPECIFIED;
    mr->range[0].min.tcp.port = htons(minport);
    mr->range[0].max.tcp.port = htons(maxport);

    iptc_rule_add_target_element(r, (unsigned char*) mr, mr_size);

    retval = 1;

out:
    return retval;
error:
    free(mr);
    return 0;
}

int iptc_rule_add_reject_target(struct iptc_rule* r, enum ipt_reject_with with) {
    struct ipt_reject_info* reject = NULL;
    unsigned int size = XT_ALIGN(sizeof(struct ipt_reject_info));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    reject = (struct ipt_reject_info*) calloc(1, size);
    if(reject == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "REJECT", XT_EXTENSION_MAXNAMELEN);
    reject->with = with;

    iptc_rule_add_target_element(r, (unsigned char*) reject, size);
out:
    return 1;
}

int iptc_rule_add_mark_target(struct iptc_rule* r, unsigned int mark, unsigned int mask) {
    struct xt_mark_tginfo2* info = NULL;
    unsigned int size = XT_ALIGN(sizeof(struct xt_mark_tginfo2));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    info = (struct xt_mark_tginfo2*) calloc(1, size);
    if(info == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    r->t.target->u.user.revision = 2;
    strncpy(r->t.target->u.user.name, "MARK", XT_EXTENSION_MAXNAMELEN);
    info->mark = mark;
    info->mask = mask;

    iptc_rule_add_target_element(r, (unsigned char*) info, size);
out:
    return 1;
}

int iptc_rule_add_classify_target(struct iptc_rule* r, unsigned int class) {

    struct xt_classify_target_info* info = NULL;
    unsigned int size = XT_ALIGN(sizeof(struct xt_classify_target_info));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    info = (struct xt_classify_target_info*) calloc(1, size);
    if(info == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "CLASSIFY", XT_EXTENSION_MAXNAMELEN);
    info->priority = class;

    iptc_rule_add_target_element(r, (unsigned char*) info, size);

out:
    return 1;
}

int iptc_rule_add_dscp_target(struct iptc_rule* r, unsigned int dscp) {
    struct xt_DSCP_info* info = NULL;
    unsigned int size = XT_ALIGN(sizeof(struct xt_DSCP_info));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    info = (struct xt_DSCP_info*) calloc(1, size);
    if(info == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "DSCP", XT_EXTENSION_MAXNAMELEN);
    info->dscp = dscp;

    iptc_rule_add_target_element(r, (unsigned char*) info, size);
out:
    return 1;
}

int iptc_rule_add_pbit_target(struct iptc_rule* r, unsigned int pbit) {
    struct xt_classify_target_info* info = NULL;
    unsigned int size = XT_ALIGN(sizeof(struct xt_classify_target_info));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    info = (struct xt_classify_target_info*) calloc(1, size);
    if(info == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "CLASSIFY", XT_EXTENSION_MAXNAMELEN);
    info->priority = pbit;

    iptc_rule_add_target_element(r, (unsigned char*) info, size);
out:
    return 1;
}

int iptc_rule_add_jump_target(struct iptc_rule* r, const char* t) {
    struct xt_standard_target* target = NULL;
    unsigned int s = XT_ALIGN(sizeof(struct xt_standard_target));

    when_null(r, out);

    target = (struct xt_standard_target*) calloc(1, s);
    when_null(target, out);

    target->target.u.target_size = s;
    snprintf(target->target.u.user.name, XT_EXTENSION_MAXNAMELEN, "%s", t);

    r->target_size = s;
    r->t.target = (struct xt_entry_target*) target;
    r->target_list_size = 0;
out:
    return 1;
}

int iptc_rule_add_snat_target(struct iptc_rule* r, unsigned long ip, unsigned int minport,
                              unsigned int maxport) {
    MULTI_RANGE* mr = NULL;
    unsigned int mr_size = 0;

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    mr = multi_range_calloc(&mr_size);
    if(mr == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "SNAT", XT_EXTENSION_MAXNAMELEN);
    mr->rangesize = 1;
    mr->range[0].flags = 1; // IP_NAT_RANGE_MAP_IPS

    /* Do not support a range of ip addresses */
    mr->range[0].min_ip = ip;
    mr->range[0].max_ip = ip;

    if(minport > 0) {
        mr->range[0].flags |= IP_NAT_RANGE_PROTO_SPECIFIED;
        mr->range[0].min.tcp.port = htons(minport);
        mr->range[0].max.tcp.port = (maxport > minport) ? htons(maxport) : htons(minport);
    }

    iptc_rule_add_target_element(r, (unsigned char*) mr, mr_size);
out:
    return 1;
}

int iptc_rule_add_dnat_target(struct iptc_rule* r, unsigned long ip, unsigned int minport,
                              unsigned int maxport) {
#if LINUX_VERSION_CODE >= KERNEL_VERSION(4, 18, 0)
    struct nf_nat_range2* range = NULL;
#else
    struct nf_nat_ipv4_multi_range_compat* range = NULL;
#endif
    const unsigned int size = XT_ALIGN(sizeof(*range));
    uint32_t min = 0;
    uint32_t max = 0;
    uint32_t flags = 0;

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    range = calloc(1, size);
    if(range == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "DNAT", XT_EXTENSION_MAXNAMELEN);

    flags |= NF_NAT_RANGE_MAP_IPS;

    if(minport > 0) {
        flags |= NF_NAT_RANGE_PROTO_SPECIFIED;
        min = htons(minport);
        max = (maxport > minport) ? htons(maxport) : min;
    }

#if LINUX_VERSION_CODE < KERNEL_VERSION(4, 18, 0)
    range->rangesize = 1;
    range->range[0].flags = flags;

    if(flags & NF_NAT_RANGE_MAP_IPS) {
        range->range[0].min_ip = ip;
        range->range[0].max_ip = ip;
    }

    if(flags & NF_NAT_RANGE_PROTO_SPECIFIED) {
        range->range[0].min.tcp.port = min;
        range->range[0].max.tcp.port = max;
    }
#else
    r->t.target->u.user.revision = 2;

    range->flags = flags;

    if(flags & NF_NAT_RANGE_MAP_IPS) {
        range->min_addr.ip = ip;
        range->max_addr.ip = ip;
    }

    if(flags & NF_NAT_RANGE_PROTO_SPECIFIED) {
        range->min_proto.tcp.port = min;
        range->max_proto.tcp.port = max;
    }
#endif

    iptc_rule_add_target_element(r, (unsigned char*) range, size);
out:
    return 1;
}

int iptc_rule_add_redirect_target(struct iptc_rule* r) {
    MULTI_RANGE* mr = NULL;
    unsigned int mr_size = 0;

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    mr = multi_range_calloc(&mr_size);
    if(mr == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "REDIRECT", XT_EXTENSION_MAXNAMELEN);
    mr->rangesize = 1;
    mr->range[0].flags = 0; // IP_NAT_RANGE_MAP_IPS·

    iptc_rule_add_target_element(r, (unsigned char*) mr, mr_size);
out:
    return 1;
}

int iptc_rule_add_redirect6_target(struct iptc_rule* r) {
#if LINUX_VERSION_CODE > KERNEL_VERSION(3, 9, 0)
    struct nf_nat_range* mr = NULL;
    unsigned int mr_size = XT_ALIGN(sizeof(struct nf_nat_range));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    mr = (struct nf_nat_range*) calloc(1, mr_size);
    if(mr == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "REDIRECT", XT_EXTENSION_MAXNAMELEN);
    mr->flags = 0; // IP_NAT_RANGE_MAP_IPS·
    iptc_rule_add_target_element(r, (unsigned char*) mr, mr_size);
out:
#else
    (void) r;
#endif
    return 1;
}

int iptc_rule_add_nfqueue_target(struct iptc_rule* r, unsigned int qnum, unsigned int qtotal, unsigned int flags) {
    unsigned char* info = NULL;
    unsigned int info_size = 0;
    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "NFQUEUE", XT_EXTENSION_MAXNAMELEN);

#if LINUX_VERSION_CODE >= KERNEL_VERSION(3, 10, 0)
    {
        struct xt_NFQ_info_v3* info_v3 = NULL;
        info_size = XT_ALIGN(sizeof(struct xt_NFQ_info_v3));

        info_v3 = (struct xt_NFQ_info_v3*) calloc(1, info_size);
        if(info_v3 == NULL) {
            free(r->t.target);
            r->t.target = NULL;
            goto out;
        }

        r->t.target->u.user.revision = 3;
        info_v3->queuenum = qnum;
        info_v3->queues_total = qtotal;
        info_v3->flags = flags;
        info = (unsigned char*) info_v3;
    }
#else
    {
        struct xt_NFQ_info_v2* info_v2 = NULL;
        info_size = XT_ALIGN(sizeof(struct xt_NFQ_info_v2));

        info_v2 = (struct xt_NFQ_info_v2*) calloc(1, info_size);
        if(info_v2 == NULL) {
            free(r->t.target);
            r->t.target = NULL;
            goto out;
        }

        r->t.target->u.user.revision = 2;
        info_v2->queuenum = qnum;
        info_v2->queues_total = qtotal;
        info_v2->bypass = flags & FW_RULE_NFQ_FLAG_BYPASS;
        info = (unsigned char*) info_v2;
    }
#endif

    iptc_rule_add_target_element(r, (unsigned char*) info, info_size);
out:
    return 1;
}

int iptc_rule_add_nflog_target(struct iptc_rule* r, unsigned int group, unsigned long len, unsigned int threshold, unsigned int flags, const char* prefix) {
    struct xt_nflog_info* info = NULL;
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_nflog_info));

    when_null(r, out);

    r->t.target = (struct xt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    info = (struct xt_nflog_info*) calloc(1, info_size);
    if(info == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "NFLOG", XT_EXTENSION_MAXNAMELEN);
    info->len = len;
    info->group = group;
    info->threshold = threshold;
    info->flags = flags;
    strcpy(info->prefix, prefix);

    iptc_rule_add_target_element(r, (unsigned char*) info, info_size);
out:
    return 1;
}

int iptc_rule_add_log_target(struct iptc_rule* r, unsigned char level, unsigned char logflags, const char* prefix) {
    struct xt_log_info* info = NULL;
    unsigned int info_size = XT_ALIGN(sizeof(struct xt_log_info));

    when_null(r, out);

    r->t.target = (struct ipt_entry_target*) calloc(1, target_size);
    when_null(r->t.target, out);

    info = (struct xt_log_info*) calloc(1, info_size);
    if(info == NULL) {
        free(r->t.target);
        r->t.target = NULL;
        goto out;
    }

    r->target_size = target_size;
    strncpy(r->t.target->u.user.name, "LOG", XT_EXTENSION_MAXNAMELEN);
    info->level = level;
    info->logflags = logflags;
    strncpy(info->prefix, prefix, sizeof(info->prefix) - 1);

    iptc_rule_add_target_element(r, (unsigned char*) info, info_size);
out:
    return 1;
}
