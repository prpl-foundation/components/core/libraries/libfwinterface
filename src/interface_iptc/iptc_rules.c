/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <arpa/inet.h>

#include <libiptc/libxtc.h>
#include <libiptc/libiptc.h>
#include <libiptc/libip6tc.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc_llist.h>

#include "fw_interface.h"
#include "interface_iptc/iptc_rules.h"

#define typeof __typeof__

struct iptc_rule* iptc_rule_new(bool ipv4) {
    unsigned int size;
    struct iptc_rule* r = NULL;

    r = (struct iptc_rule*) calloc(1, sizeof(struct iptc_rule));
    when_null(r, exit);

    if(ipv4) {
        size = XT_ALIGN(sizeof(struct ipt_entry));
        r->e.entry = (struct ipt_entry*) calloc(1, size);
        if(r->e.entry == NULL) {
            free(r);
            return NULL;
        }
    } else {
        size = XT_ALIGN(sizeof(struct ip6t_entry));
        r->e.entry6 = (struct ip6t_entry*) calloc(1, size);
        if(r->e.entry6 == NULL) {
            free(r);
            return NULL;
        }
    }

    r->entry_size = size;
    amxc_llist_init(&r->match_list);
    amxc_llist_init(&r->target_list);

exit:
    return r;
}

static void iptc_rule_delete_list(amxc_llist_it_t* it) {
    struct element* element = amxc_container_of(it, struct element, it);
    free(element->elem);
    free(element);
}

void iptc_rule_delete(struct iptc_rule* r, bool ipv4) {
    when_null(r, exit);

    if(ipv4) {
        free(r->e.entry);
        r->e.entry = NULL;
        free(r->t.target);
        r->t.target = NULL;
    } else {
        free(r->e.entry6);
        r->e.entry6 = NULL;
        free(r->t.target6);
        r->t.target6 = NULL;
    }

    r->entry_size = 0;
    r->target_size = 0;

    amxc_llist_clean(&r->match_list, iptc_rule_delete_list);
    amxc_llist_clean(&r->target_list, iptc_rule_delete_list);

    r->match_list_size = 0;
    r->target_list_size = 0;

    free(r);

exit:
    return;
}

int iptc_rule_add_target_element(struct iptc_rule* r, unsigned char* m, unsigned int size) {
    struct element* e = NULL;

    when_null(r, exit);
    when_null(m, exit);

    if(amxc_llist_is_empty(&r->target_list)) {
        e = (struct element*) calloc(1, sizeof(struct element));
        when_null(e, exit);

        e->elem_size = size;
        e->elem = m;

        r->target_list_size += size;
        amxc_llist_append(&r->target_list, &e->it);
    }

exit:
    return 1;
}

int iptc_rule_add_match_element(struct iptc_rule* r, unsigned char* m, unsigned int size) {
    struct element* e = NULL;

    when_null(m, exit);

    e = (struct element*) calloc(1, sizeof(struct element));
    when_null(e, exit);

    e->elem_size = size;
    e->elem = m;

    r->match_list_size += size;
    amxc_llist_append(&r->match_list, &e->it);

exit:
    return 1;
}

struct ipt_entry* iptc_rule_generate_entry(struct iptc_rule* r) {
    unsigned int total_size = 0;
    struct ipt_entry* e = NULL;
    amxc_llist_it_t* first = NULL;
    unsigned int s = 0;

    when_null(r, exit);
    when_null(r->t.target, exit);
    when_null(r->e.entry, exit);

    total_size = r->entry_size + r->match_list_size + r->target_size + r->target_list_size;

    SAH_TRACEZ_INFO(ME, "Create ipt_entry, [%u]bytes", total_size);

    e = (struct ipt_entry*) calloc(1, total_size);
    when_null(e, exit);

    SAH_TRACEZ_INFO(ME, "Copy struct ipt_entry [%u]bytes", r->entry_size);
    r->e.entry->target_offset = r->entry_size + r->match_list_size;
    r->e.entry->next_offset = total_size;
    memcpy(e, (unsigned char*) r->e.entry, r->entry_size);

    //copy additional match targets (supports more than one)
    amxc_llist_for_each(it, &r->match_list) {
        struct element* element = (struct element*) amxc_llist_it_get_data(it, struct element, it);

        memcpy(e->elems + s, element->elem, element->elem_size);
        s += element->elem_size;
    }

    //copy target
    r->t.target->u.target_size = r->target_size + r->target_list_size;
    memcpy(e->elems + r->match_list_size, (unsigned char*) r->t.target, r->target_size);
    SAH_TRACEZ_INFO(ME, "Base target[%u] + total target [%u]bytes", r->target_size, r->t.target->u.target_size);

    //copy additional target data
    //TODO, support list.
    if(!amxc_llist_is_empty(&r->target_list)) {
        first = amxc_llist_get_first(&r->target_list);
        struct element* element = (struct element*) amxc_llist_it_get_data(first, struct element, it);
        memcpy(e->elems + r->match_list_size + r->target_size, element->elem, r->target_list_size);
    }

exit:
    return e;
}

struct ip6t_entry* iptc_rule_generate_entry6(struct iptc_rule* r) {
    unsigned int total_size = 0;
    struct ip6t_entry* e = NULL;
    amxc_llist_it_t* first = NULL;
    unsigned int s = 0;

    when_null(r, exit);
    when_null(r->t.target6, exit);
    when_null(r->e.entry6, exit);

    total_size = r->entry_size + r->match_list_size + r->target_size + r->target_list_size;

    SAH_TRACEZ_INFO(ME, "Create ip6t_entry, [%u]bytes", total_size);

    e = (struct ip6t_entry*) calloc(1, total_size);
    when_null(e, exit);

    SAH_TRACEZ_INFO(ME, "Copy struct ip6t_entry [%u]bytes", r->entry_size);
    r->e.entry6->target_offset = r->entry_size + r->match_list_size;
    r->e.entry6->next_offset = total_size;
    memcpy(e, (unsigned char*) r->e.entry6, r->entry_size);

    //copy additional match targets (supports more than one)
    amxc_llist_for_each(it, &r->match_list) {
        struct element* element = (struct element*) amxc_llist_it_get_data(it, struct element, it);

        memcpy(e->elems + s, element->elem, element->elem_size);
        s += element->elem_size;
    }

    //copy target
    r->t.target6->u.target_size = r->target_size + r->target_list_size;
    memcpy(e->elems + r->match_list_size, (unsigned char*) r->t.target6, r->target_size);
    SAH_TRACEZ_INFO(ME, "Base target[%u] + total target [%u]bytes", r->target_size, r->t.target6->u.target_size);

    //copy additional target data
    //TODO, support list.
    if(!amxc_llist_is_empty(&r->target_list)) {
        first = amxc_llist_get_first(&r->target_list);
        struct element* element = (struct element*) amxc_llist_it_get_data(first, struct element, it);
        memcpy(e->elems + r->match_list_size + r->target_size, element->elem, r->target_list_size);
    }

exit:
    return e;
}

