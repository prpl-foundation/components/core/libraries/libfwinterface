/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <errno.h>
#include <stdlib.h>
#include <string.h>

#include <amxc/amxc_macros.h>
#include <fwinterface/interface.h>
#include "fw_interface.h"
#include "interface_iptc/iptc.h"
#include "interface_iptc/iptc_rules.h"

int fw_add_chain(const char* chain, const char* table, bool is_ipv6) {
    int retval = -1;
    struct xtc_handle* handle = NULL;

    when_null(chain, exit);
    when_null(table, exit);

    handle = get_iptc_handle(table, is_ipv6);
    when_null(handle, exit);

    retval = (is_ipv6 ? ip6tc_create_chain(chain, handle) : iptc_create_chain(chain, handle));

    if(!retval) {
        if(EEXIST == errno) {
            SAH_TRACEZ_INFO(ME, "Table[%s] already has chain[%s], flush entries", table, chain);
            (is_ipv6 ? ip6tc_flush_entries(chain, handle) : iptc_flush_entries(chain, handle));
            retval = 0;
            goto exit;

        } else {
            SAH_TRACEZ_ERROR(ME, "Failed to create table[%s], chain[%s]: %s",
                             table, chain, iptc_strerror(errno));
            retval = -1;
            goto exit;
        }
    }

    SAH_TRACEZ_INFO(ME, "Created table[%s], chain[%s]", table, chain);
    retval = 0;

exit:
    return retval;
}

static int fw_append_or_insert_rule(const fw_rule_t* const rule, bool insert, uint32_t index) {
    int retval = -1;
    bool is_ipv6 = false;
    const char* table = NULL;
    const char* chain = NULL;
    struct xtc_handle* handle = NULL;
    struct iptc_rule* iptc_rule = NULL;
    fw_rule_target_t target = FW_RULE_TARGET_NONE;

    when_null(rule, exit);

    if(insert) {
        when_true(index == 0, exit);
        index--; //lib_iptc counts indices from zero.
    }

    table = fw_rule_get_table(rule);
    chain = fw_rule_get_chain(rule);
    target = fw_rule_get_target(rule);

    // Minimum rule requirements
    when_str_empty(table, exit);
    when_str_empty(chain, exit);
    when_true(((target == FW_RULE_TARGET_NONE) || (target == FW_RULE_TARGET_LAST)), exit);

    is_ipv6 = fw_rule_get_ipv6(rule);

    handle = get_iptc_handle(table, is_ipv6);
    when_null(handle, exit);

    iptc_rule = iptc_create_rule(rule);
    when_null(iptc_rule, exit);

    retval = 0;

    if(is_ipv6) {
        struct ip6t_entry* entry = iptc_rule_generate_entry6(iptc_rule);
        when_null(entry, out);

        if(insert) {
            retval = ip6tc_insert_entry(chain, entry, index, handle);
        } else {
            retval = ip6tc_append_entry(chain, entry, handle);
        }
        free(entry);
    } else {
        struct ipt_entry* entry = iptc_rule_generate_entry(iptc_rule);
        when_null(entry, out);

        if(insert) {
            retval = iptc_insert_entry(chain, entry, index, handle);
        } else {
            retval = iptc_append_entry(chain, entry, handle);
        }
        free(entry);
    }

out:
    iptc_rule_delete(iptc_rule, !is_ipv6);

    if(!retval) {
        SAH_TRACEZ_ERROR(ME, "Failed to %s rule to table[%s], chain[%s], index[%d]: %s",
                         (insert ? "insert" : "append"), table, chain, index, iptc_strerror(errno));

        retval = -1;
    } else {
        SAH_TRACEZ_INFO(ME, "%s rule to table[%s], chain[%s] index[%d]", (insert ? "Inserted" : "Appended"), table, chain, index);

        retval = 0;
    }

exit:
    return retval;
}

//A wrapper for now.
int fw_add_rule(const fw_rule_t* const rule, uint32_t index) {
    return fw_replace_rule(rule, index);
}

int fw_replace_rule(const fw_rule_t* const rule, uint32_t index) {
    return fw_append_or_insert_rule(rule, true, index);
}

int fw_append_rule(const fw_rule_t* const rule) {
    return fw_append_or_insert_rule(rule, false, 0);
}

int fw_delete_rule(const fw_rule_t* const rule, uint32_t index) {
    int retval = -1;
    bool is_ipv6 = false;
    const char* table = NULL;
    const char* chain = NULL;
    struct xtc_handle* handle = NULL;

    when_null(rule, exit);
    when_true(index == 0, exit);

    table = fw_rule_get_table(rule);
    chain = fw_rule_get_chain(rule);

    // Minimum rule requirements
    when_null(table, exit);
    when_null(chain, exit);

    index--; //lib_iptc counts indices from zero.
    is_ipv6 = fw_rule_get_ipv6(rule);

    handle = get_iptc_handle(table, is_ipv6);
    when_null(handle, exit);

    retval = (is_ipv6 ? ip6tc_delete_num_entry(chain, index, handle) :
              iptc_delete_num_entry(chain, index, handle));

    if(!retval) {
        SAH_TRACEZ_ERROR(ME, "Failed to delete rule from table[%s], chain[%s], index[%d]: %s",
                         table, chain, index, iptc_strerror(errno));

        retval = -1;
        goto exit;
    }

    SAH_TRACEZ_INFO(ME, "Deleted rule from table[%s], chain[%s], index[%d]", table, chain, index);
    retval = 0;

exit:
    return retval;
}

int fw_apply(void) {
    int retval = -1;

    retval = iptc_apply();

    return (retval ? 0 : -1);
}
