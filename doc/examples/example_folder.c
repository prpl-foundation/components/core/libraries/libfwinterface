/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <unistd.h>
#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

static bool callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                     const char* chain, const char* table, int index) {

    int retval = -1;
    char const* str_flag = "unknown";
    char const* str_policy = "unknown";

    switch(flag) {
    case FW_RULE_FLAG_NEW:
        str_flag = "new";
        break;
    case FW_RULE_FLAG_MODIFIED:
        str_flag = "modified";
        break;
    case FW_RULE_FLAG_DELETED:
        str_flag = "deleted";
        break;
    case FW_RULE_FLAG_HANDLED:
        str_flag = "handled";
        break;
    case FW_RULE_FLAG_LAST:
        str_flag = "last (should never get here)";
        break;
    }

    switch(fw_rule_get_target_policy_option(rule)) {
    case FW_RULE_POLICY_DROP:
        str_policy = "DROP";
        break;
    case FW_RULE_POLICY_ACCEPT:
        str_policy = "ACCEPT";
        break;
    case FW_RULE_POLICY_REJECT:
        str_policy = "REJECT";
        break;
    case FW_RULE_POLICY_MASQUERADE:
        str_policy = "MASQUERADE";
        break;
    case FW_RULE_POLICY_LAST:
        str_policy = "last (should never get here)";
        break;
    }

    printf("Callback for rule:\n");
    printf("\tFlag: %s\n", str_flag);
    printf("\tEnabled: %s\n", fw_rule_is_enabled(rule) ? "Yes" : "No");
    printf("\tChain: %s\n", chain);
    printf("\tTable: %s\n", table);
    printf("\tIndex: %d\n", index);

    //Print additional parameter values.
    printf("\tIPv4: %s\n", fw_rule_get_ipv4(rule) ? "Yes" : "No");
    printf("\tDestination port: %d\n", fw_rule_get_destination_port(rule));
    printf("\tPolicy: %s\n", str_policy);

    //Print feature parameter values.
    printf("\tIngress interface: %s\n", fw_rule_get_in_interface(rule));

    //Check the flag and invoke the appropriate libfwinterface function.
    if(FW_RULE_FLAG_DELETED == flag) {
        printf("Deleting the rule ... ");
        retval = fw_delete_rule(rule, index);
        printf("%s\n", (retval ? "failed" : "success"));

    } else if((FW_RULE_FLAG_MODIFIED == flag) || (FW_RULE_FLAG_NEW == flag)) {
        printf("Adding the rule ... ");
        retval = fw_add_rule(rule, index);
        printf("%s\n", (retval ? "failed" : "success"));
    } else {
        printf("Invalid flag\n");
    }

    return (retval ? false : true);
}

static int set_ingress_interface(fw_folder_t* folder, const char* interface) {
    int ret = 0;
    int retval = -1;
    fw_rule_t* rule = NULL;

    if(!folder || !interface || !interface[0]) {
        ret = -1;
        goto exit;
    }

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);

    if(!rule) {
        printf("Failed to fetch a feature rule for %s\n", interface);
        ret = -1;
        goto exit;
    }

    //Set the specific parameters.
    retval = fw_rule_set_in_interface(rule, interface);

    if(retval) {
        printf("Failed to set ingress interface %s\n", interface);
        ret = -1;
    }

    //Push the rule back to mark it ready, even after a failure.
    retval = fw_folder_push_rule(folder, rule);

    if(retval) {
        printf("Failed to push rule back to folder for interface %s\n", interface);
        ret = -1;
    }

exit:
    return ret;
}

int main(int argc, const char* argv[]) {
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;
    int retval = 0;

    retval = fw_folder_new(&folder);

    if(retval) {
        printf("Failed to create folder\n");
        return 1;
    }

    retval = fw_folder_set_enabled(folder, true);

    if(retval) {
        printf("Failed to enabled folder\n");
        goto exit;
    }

    //Set features.
    retval = fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);

    if(retval) {
        printf("Failed to set feature FW_FEATURE_IN_INTERFACE\n");
        goto exit;
    }

    rule = fw_folder_fetch_default_rule(folder);

    if(!rule) {
        printf("Failed to fetch the default rule\n");
        goto exit;
    }

    printf("Default rule is fetched\n");

    retval |= fw_rule_set_chain(rule, "INPUT");
    retval |= fw_rule_set_table(rule, "filter");

    //Set parameters to allow IPv4 traffic to port 22.
    retval |= fw_rule_set_ipv4(rule, true);
    retval |= fw_rule_set_destination_port(rule, 22);
    retval |= fw_rule_set_target_policy(rule, FW_RULE_POLICY_ACCEPT);

    if(retval) {
        printf("Failed to set rule parameters\n");
        goto exit;
    }

    retval = fw_folder_push_rule(folder, rule);
    rule = NULL; //Do not use the rule pointer after the push.

    if(retval) {
        printf("Failed to push the rule in the folder\n");
        goto exit;
    }

    //The default rule is set above. Now, three features rules are required, one for each eth interface.
    printf("Setting feature rules for eth0, eth1 and eth2\n");
    retval |= set_ingress_interface(folder, "eth0");
    retval |= set_ingress_interface(folder, "eth1");
    retval |= set_ingress_interface(folder, "eth2");

    if(retval) {
        printf("Failed to add one or more feature rules\n");
        goto exit;
    }

    retval = fw_commit(callback);

    if(retval) {
        printf("Failed to commit changes\n");
        goto exit;
    }

    //The callback function has changed the firewall's configuration. These
    //changes are still cached. Write them to the firewall.
    retval = fw_apply();

    if(retval) {
        printf("Failed to write the new configuration to the firewall\n");
        goto exit;
    }

    printf("Firewall rules are added. Press a key to delete them\n");
    getchar();

    printf("Deleting all folder rules\n");
    retval = fw_folder_delete_rules(folder);

    if(retval) {
        printf("Failed to mark all folder rules for deletion\n");
        goto exit;
    }

    retval = fw_commit(callback);

    if(retval) {
        printf("Failed to commit changes\n");
    }

exit:
    retval = fw_folder_delete(&folder);

    if(retval) {
        printf("Failed to delete folder\n");
        return 1;
    }

    //Make sure the rules are actually deleted from the system and the memory is freed.
    retval = fw_commit(callback);

    if(retval) {
        printf("Failed to commit changes\n");
    }

    //The callback function has changed the firewall's configuration. These
    //changes are still cached. Write them to the firewall.
    //Note that fw_apply() can be called after several fw_commit() invocations.
    retval = fw_apply();

    if(retval) {
        printf("Failed to write the new configuration to the firewall\n");
    }

    return 0;
}
